package io.gitlab.mihajlonesic.jsp.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MVCDemoServlet", urlPatterns = {"/MVCDemoServlet"})
public class MVCDemoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Step 0: Add data
        String[] students = {"John", "Jake", "Jimmy"};
        request.setAttribute("student_list", students);
        
        // Step 1: Get request dispatcher
        RequestDispatcher dispatcher = request.getRequestDispatcher("/mvc-view-students.jsp");
        
        // Step 2: Forward the request to JSP
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // not implemented
    }

}