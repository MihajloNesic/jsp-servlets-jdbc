package io.gitlab.mihajlonesic.jsp.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ParamServlet", urlPatterns = {"/ParamServlet"})
public class ParamServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // set content type
        response.setContentType("text/html;charset=UTF-8");
        
        // get printwriter
        PrintWriter out = response.getWriter();
        
        // read configuration params
        ServletContext context = getServletContext(); // inherit from HttpServlet
        
        String maxShoppingCartSize = context.getInitParameter("max-shopping-cart-size"); // from web.xml
        String projectTeamName = context.getInitParameter("project-team-name"); // from web.xml
        
        // generate HTML code
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlets - ParamServlet</title>");            
        out.println("</head>");
        out.println("<body>");
        out.println("<p>Max shopping cart size: <b>" + maxShoppingCartSize + "</b></p>");
        out.println("<p>Project team name: <b>" + projectTeamName + "</b></h3>");
        out.println("</body>");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // not implemented
    }

}