package io.gitlab.mihajlonesic.jsp.jdbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet(name = "StudentControllerServlet", urlPatterns = {"/StudentControllerServlet"})
public class StudentControllerServlet extends HttpServlet {

    private StudentDBUtil studentDBUtil;
    
    @Resource(name="jdbc/udemy_jsp")
    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        
        // create our student db util and pass in the conn pool / datasource
        try {
            studentDBUtil = new StudentDBUtil(dataSource);
        }
        catch(Exception ex) {
            throw new ServletException(ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // read the "command" parameter
            String command = request.getParameter("command");
            
            // if the command is missing, then list students
            if(command == null)
                command = "LIST";
            
            // route to the appropriate method
            switch(command) {
                case "LIST": 
                    // list the students (MVC)
                    listStudents(request, response);
                    break;
                case "LOAD":
                    loadStudent(request, response);
                    break;
                case "ADD": 
                    addStudent(request, response);
                    break;
                case "UPDATE": 
                    updateStudent(request, response);
                    break;
                case "DELETE": 
                    deleteStudent(request, response);
                    break;   
                default:
                    listStudents(request, response); // or an error message
            }
        }
        catch(Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    
    private void listStudents(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        // get students from db util
        List<Student> students = studentDBUtil.getStudents();
            
        // add students to the request
        request.setAttribute("student_list", students);
        
        // send it to JSP (View)
        RequestDispatcher dispatcher = request.getRequestDispatcher("/jdbc-list-students.jsp");
        dispatcher.forward(request, response);
    }

    private void loadStudent(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        // read student id from form data
        String studentID = request.getParameter("studentID");
        
        // get student from database
        Student student = studentDBUtil.getStudent(studentID);
        
        // place student in the request attribute
        request.setAttribute("passed_student", student);
        
        // send to jsp page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/jdbc-update-student.jsp");
        dispatcher.forward(request, response);
    }
    
    private void addStudent(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        // read student info from form data
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        
        // create a new Student object
        Student student = new Student(firstName, lastName, email);
        
        // add the student to the database
        studentDBUtil.addStudent(student);
        
        // send back to main page (the student list)
        listStudents(request, response);
    }

    private void updateStudent(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        // read student from form data
        int id = Integer.parseInt(request.getParameter("studentID"));
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        
        // create a new student object
        Student student = new Student(id, firstName, lastName, email);
        
        // perform update on database
        studentDBUtil.updateStudent(student);
        
        // send back to main page (the student list)
        listStudents(request, response);
    }

    private void deleteStudent(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        // read student id from form data
        int studentID = Integer.parseInt(request.getParameter("studentID"));
        
        // perform delete on database
        studentDBUtil.deleteStudent(studentID);
        
        // send back to main page (the student list)
        listStudents(request, response);
    }
}