package io.gitlab.mihajlonesic.jsp.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet(name = "ConnectionTestServlet", urlPatterns = {"/ConnectionTestServlet"})
public class ConnectionTestServlet extends HttpServlet {
    
    // Define datasource/connection pool for Resource Injection
    @Resource(name="jdbc/udemy_jsp")
    private DataSource dataSource;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        // Set up printwriter
        PrintWriter out = response.getWriter();
        
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet ConnectionTestServlet</title>");            
        out.println("</head>");
        out.println("<body>");
        
        // Get a connection to the database
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        
        try {
            conn = dataSource.getConnection();
            
            out.println("<h3>Connected to database successfully</h3>");
            
            // Create a sql statement
            String query = "SELECT * FROM STUDENT;";
            statement = conn.createStatement();
            
            // execute sql query
            resultSet = statement.executeQuery(query);
            
            // process the result set
            while(resultSet.next()) {
                String email = resultSet.getString("email");
                out.println("<p>"+email+"</p>");
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
            out.println("<h3>Connection to database failed</h3>");
        }
        finally {
            out.println("</body>");
            out.println("</html>");
        }
    }

}