package io.gitlab.mihajlonesic.jsp.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

public class StudentDBUtil {
    private DataSource dataSource;

    public StudentDBUtil(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public List<Student> getStudents() throws SQLException {
        List<Student> students = new ArrayList<>();
        
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        
        try {
            // get a connection to database
            conn = dataSource.getConnection();
            
            // create a sql statement
            String query = "SELECT * FROM student ORDER BY last_name;";
            statement = conn.createStatement();
            
            // execute query
            resultSet = statement.executeQuery(query);
            
            // process result set
            while(resultSet.next()) {
                // retrieve data from result set row
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                
                // create a new Student object
                Student student = new Student(id, firstName, lastName, email);
                
                // add it to the list of students
                students.add(student);
            }
            
            return students;
        }
        finally {
            // close JDBC objects
            close(conn, statement, resultSet);
        }
    }
    
    public void addStudent(Student student) throws SQLException {
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        
        try {
            // get database connection
            conn = dataSource.getConnection();
            
            // create query for insert
            String queryInsertStudent = "INSERT INTO student (first_name, last_name, email) VALUES (?, ?, ?);";
            ps = conn.prepareStatement(queryInsertStudent, Statement.RETURN_GENERATED_KEYS);
            
            // set the param values for the student
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setString(3, student.getEmail());

            // execute query
            ps.executeUpdate();

            // get auto generated ID for the student
            resultSet = ps.getGeneratedKeys();
            
            while(resultSet.next()) {
                int new_student_id = resultSet.getInt(1);
                student.setId(new_student_id);
            }
        }
        finally {
            // clean up JDB objects
            close(conn, ps, resultSet);
        }
    }
    
    public Student getStudent(String studentID) throws SQLException {
        Student student = null;
        
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        int studentIDInt;
        
        try {
            // convert student id to int
            studentIDInt = Integer.parseInt(studentID);
            
            // get connection to database
            conn = dataSource.getConnection();
            
            // create query to get selected student
            String querySelectStudent = "SELECT * FROM student WHERE id=?;";
            
            // create prepared statement
            ps = conn.prepareStatement(querySelectStudent);
            
            // set params
            ps.setInt(1, studentIDInt);
            
            // execute statement
            resultSet = ps.executeQuery();
            
            // retrieve data from result set
            if(resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                
                // passing student ID during construction
                student = new Student(studentIDInt, firstName, lastName, email);
            }
            else throw new SQLException("Could not find student with id: "+studentID);
            
            return student;
        }
        finally {
            // clean up JDB objects
            close(conn, ps, resultSet);
        }
    }
    
    public void updateStudent(Student student) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            // get database connection
            conn = dataSource.getConnection();

            // create query for update
            String queryUpdateStudent = "UPDATE student SET first_name=?, last_name=?, email=? WHERE id=?;";

            // prepare statement
            ps = conn.prepareStatement(queryUpdateStudent);

            // set params
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setString(3, student.getEmail());
            ps.setInt(4, student.getId());

            // execute query
            ps.execute();
        }
        finally {
            // clean up JDB objects
            close(conn, ps, null);
        }
    }
    
    public void deleteStudent(int studentID) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            // get database connection
            conn = dataSource.getConnection();

            // create query for delete
            String queryDeleteStudent = "DELETE FROM student WHERE id=?;";

            // prepare statement
            ps = conn.prepareStatement(queryDeleteStudent);

            // set params
            ps.setInt(1, studentID);

            // execute query
            ps.execute();
        }
        finally {
            // clean up JDB objects
            close(conn, ps, null);
        }
    }
    
    private void close(Connection conn, Statement statement, ResultSet resultSet) {
        try {
            if(resultSet != null)
                resultSet.close();
            
            if(statement != null)
                statement.close();
            
            if(conn != null)
                conn.close(); // doesn't really close it, just puts back in connection pool (make available for someone else to use)
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
    }
}