package io.gitlab.mihajlonesic.jsp.tagdemo;

/**
 * @author Mihajlo
 */
public class City {
    
    private String name;
    private String country;
    private int population;
    private boolean visited;

    public City(String name, String country, int population, boolean visited) {
        this.name = name;
        this.country = country;
        this.population = population;
        this.visited = visited;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
    
}