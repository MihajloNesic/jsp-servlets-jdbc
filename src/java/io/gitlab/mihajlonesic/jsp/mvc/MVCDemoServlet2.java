package io.gitlab.mihajlonesic.jsp.mvc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MVCDemoServlet2", urlPatterns = {"/MVCDemoServlet2"})
public class MVCDemoServlet2 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Step 1: Get data from helper class (model)
        List<Student> students = StudentDataUtil.getStudents();
        
        // Step 2: Add data to request object
        request.setAttribute("student_list", students);
        
        // Step 3: Get request dispatcher
        RequestDispatcher dispatcher = request.getRequestDispatcher("/mvc-view-students2.jsp");
         
        // Step 4: Forward the request to JSP
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // not implemented
    }

}
