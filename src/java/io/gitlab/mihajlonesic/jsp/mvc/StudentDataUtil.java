package io.gitlab.mihajlonesic.jsp.mvc;

import java.util.ArrayList;
import java.util.List;

public class StudentDataUtil {
    
    public static List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        
        students.add(new Student("John", "Doe", "john.doe@test.com"));
        students.add(new Student("Mary", "Test", "maryt@demo.co"));
        students.add(new Student("Jimmy", "Butler", "jimmy@nba.com"));
        
        return students;
    }
}