package io.gitlab.mihajlonesic.jsp;

import java.util.Random;

/**
 * @author Mihajlo
 */
public class RandomString {

    private static final Random RANDOM = new Random();
    
    private static final String NUMERIC = "0123456789";
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final String ALPHA_NUMERIC = NUMERIC + ALPHABET;
    
    /**
     * Generates a random String of letters
     * @param length Length of the generated String
     * @return Random String of length n
     */
    public static String randomString(int length){
        return generateString(ALPHABET, length);
    }
    
    /**
     * Generates a random String of letters and numbers
     * @param length Length of the generated String
     * @return Random String of length n
     */
    public static String randomAlphaNumeric(int length){
        return generateString(ALPHA_NUMERIC, length);
    }
    
    /**
     * Generates a random String of numbers
     * @param length Length of the generated String
     * @return Random String of length n
     */
    public static String randomNumeric(int length){
        return generateString(NUMERIC, length);
    }
    
    private static String generateString(String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
            text[i] = characters.charAt(RANDOM.nextInt(characters.length()));
        return new String(text);
    }

}