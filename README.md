## [JSP, Servlets and JDBC Course](https://www.udemy.com/jsp-tutorial/)

Libraries needed:
* JSTL ([download bundle](http://www.luv2code.com/download-jstl))
    * [javax.servlet.jsp.jstl-1.2.1](https://mvnrepository.com/artifact/org.glassfish.web/javax.servlet.jsp.jstl/1.2.1)
    * [javax.servlet.jsp.jstl-api-1.2.1](https://mvnrepository.com/artifact/javax.servlet.jsp.jstl/javax.servlet.jsp.jstl-api/1.2.1)
* MySQL JDBC Connector
    * [mysql-connector-java-5.1.46.jar](https://dev.mysql.com/downloads/connector/j/)

---

For *JDBC CRUD app*, the database `udemy_jsp` is required.<br>
The only table in the database is table `student`

![student table](https://i.imgur.com/8Gpv6Rs.png)