<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Student Response</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <jsp:include page="header.html" />
        
        <div align="center">
            <h3>The student is confirmed: ${param.firstName} ${param.lastName}</h3>
            
            <h4 style="margin-bottom: 0;">Favorite languages:</h4>
            <ul style="list-style: none; padding: 0; margin: 0;">
                <%
                    String[] favLangs = request.getParameterValues("favoriteLanguage");

                    for(String lang : favLangs) {
                        out.print("<li>"+lang+"</li>");
                    }
                %>
            </ul>
        </div>
        
        <jsp:include page="footer.jsp" />
    </body>
</html>
