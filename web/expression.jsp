<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Expresson</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <p>The time on the server is <%= new java.util.Date() %></p>
        <p>Convering a string to uppercase:  <%= new String("Hello World").toUpperCase() %></p>
        <p>25 * 4 = <%= 25 * 4 %></p>
        <p>Is 75 less than 60? <%= 75 < 60 %></p>
    </body>
</html>
