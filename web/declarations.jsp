<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Declarations</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%!
            int gcd(int a, int b) {
                if(a<b)
                    return gcd(b, a);
                
                while(b != 0) {
                    int t = b;
                    b = a % b;
                    a = t;
                }
                
                return a;
            }
        %>
        
        <p>gcd(16, 4) = <%= gcd(16, 4) %></p>
        <p>gcd(232, 22) = <%= gcd(232, 22) %></p>
        <p>gcd(144, 12) = <%= gcd(144, 12) %></p>
    </body>
</html>
