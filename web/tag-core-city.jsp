<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.*,io.gitlab.mihajlonesic.jsp.tagdemo.City"%>

<%
    // sample data (normaly provided by MVC)
    List<City> data = new ArrayList<>();
            
    data.add(new City("Madrid", "Spain", 3166000, true));
    data.add(new City("Amsterdam", "Netherlands", 821000, false));
    data.add(new City("Brussels", "Belgium", 1200000, true));
    data.add(new City("Hamburg", "Germany", 1810000, false));
            
    pageContext.setAttribute("cities", data);
%>

<!DOCTYPE html>
<html>
    <head>
        <title>JSTL Core Tag - City</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table border="1">
            <tr>
                <th>City name</th>
                <th>Country name</th>
                <th>Population</th>
                <th>Over 1.5 mill</th>
                <th>Visited</th>
            </tr>
            <!-- Core FOR EACH statement -->
            <c:forEach var="city" items="${cities}">
                <tr>
                    <td>${city.name}</td>
                    <td>${city.country}</td>
                    <td>${city.population}</td>
                    <!-- Core IF statement -->
                    <c:if test="${city.population >= 1500000}">
                        <td>yes</td>
                    </c:if>
                    <c:if test="${!(city.population >= 1500000)}">
                        <td>no</td>
                    </c:if>
                    <!-- Core CHOOSE statement (simillar to switch in java) -->
                    <c:choose>
                        <c:when test="${city.visited}">
                            <td>yes</td>
                        </c:when>
                        <c:otherwise>
                            <td>no</td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>