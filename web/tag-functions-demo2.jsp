<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL Functions Tag - Demo 2</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <c:set var="data" value="Madrid,Prague,Bratislava,Tokio" />
        
        <c:set var="citiesArray" value="${fn:split(data, ',')}" />
        <c:set var="citiesRow" value="${fn:join(citiesArray, '-')}" />
        
        <h3>Split Demo</h3>
        
        <c:forEach var="city" items="${citiesArray}">
            ${city} <br>
        </c:forEach>
            
        <h3>Join Demo</h3>
        <p>Result of joining: ${citiesRow}</p>
    </body>
</html>