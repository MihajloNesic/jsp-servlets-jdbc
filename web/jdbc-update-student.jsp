<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JDBC CRUD app - Update Student</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/jdbc-crud-app.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <h2>Test University</h2>
            </div>
        </div>
        <div id="container">
            <h3>Update Student</h3>
            <form action="StudentControllerServlet" method="post">
                <input type="hidden" name="command" value="UPDATE"/>
                <input type="hidden" name="studentID" value="${passed_student.id}"/>
                <table>
                    <tbody>
                        <tr>
                            <td><label>First Name</label></td>
                            <td><input type="text" name="firstName" value="${passed_student.firstName}"/></td>
                        </tr>
                        <tr>
                            <td><label>Last Name</label></td>
                            <td><input type="text" name="lastName" value="${passed_student.lastName}"/></td>
                        </tr>
                        <tr>
                            <td><label>Email</label></td>
                            <td><input type="text" name="email" value="${passed_student.email}"/></td>
                        </tr>
                        <tr>
                            <td><label></label></td>
                            <td><input type="submit" value="Update" class="save"/></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            
            <div style="clear: both;"></div>
            
            <p><a href="StudentControllerServlet">Back to List</a></p>
            
        </div>
    </body>
</html>