<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL Functions Tag - Demo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <c:set var="data" value="GitLab" />
        
        <p>The length of the string <b>${data}</b> is: ${fn:length(data)}</p>
        <p>Uppercase version of the string <b>${data}</b> is: ${fn:toUpperCase(data)}</p>
        <p>Does the string <b>${data}</b> start with <b>git</b>: ${fn:startsWith(data.toLowerCase(), "git")}</p>
    </body>
</html>