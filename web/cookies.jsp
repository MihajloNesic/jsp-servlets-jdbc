<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Cookies - Favorite language</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h3>Training portal</h3>
        
        <!-- read the favorite programming language cookie -->
        <%
            // the default ... if there are no cookies
            String favLang = null;
            
            // get the cookies from the browser request
            Cookie[] cookies = request.getCookies();
            
            // find our cookie
            if(cookies != null) {
                for(Cookie tempCookie : cookies) {
                    if("jsptest.favLang".equals(tempCookie.getName())) {
                        favLang = tempCookie.getValue();
                        break;
                    }
                }
            }
            
            // if language is null, redirect
            if(favLang == null) {
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", "cookies-personalize.html");
            }

        %>
        
        <!-- show a personalized page -->
        
        <h4>New books for <%= favLang %></h4>
        <ul>
            <li><%= favLang %> Cookbook</li>
            <li>Programming <%= favLang %></li>
            <li>The basics of <%= favLang %></li>
        </ul>
        
        <hr>
        
        <a href="cookies-personalize.html">Personalize this page</a>
        
    </body>
</html>
