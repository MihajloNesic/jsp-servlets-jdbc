<%@page 
    contentType="text/html" pageEncoding="UTF-8"
    import="io.gitlab.mihajlonesic.jsp.RandomString"
%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Random String</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h3>Random Strings:</h3>
        <ul>
            <li>Lenght 20: <%= RandomString.randomAlphaNumeric(20) %></li>
            <li>Lenght 10: <%= RandomString.randomAlphaNumeric(10) %></li>
            <li>Lenght 5: <%= RandomString.randomAlphaNumeric(5) %></li>
        </ul>
    </body>
</html>
