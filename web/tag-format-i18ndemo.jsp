<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:set var="theLocale" value="${not empty param.theLocale ? param.theLocale : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${theLocale}" />
<fmt:setBundle basename="io.gitlab.mihajlonesic.jsp.tagdemo.i18n.mylabels" />

<!DOCTYPE html>
<html>
    <head>
        <title>JSTL Format Tag - I18N Demo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <a href="tag-format-i18ndemo.jsp?theLocale=en_US">English (US)</a> |
        <a href="tag-format-i18ndemo.jsp?theLocale=es_ES">Spanish (ES)</a> |
        <a href="tag-format-i18ndemo.jsp?theLocale=de_DE">German (DE)</a> |
        <a href="tag-format-i18ndemo.jsp?theLocale=ja_JP">Japanese (JP)</a> |
        <a href="tag-format-i18ndemo.jsp?theLocale=sr_RS">Serbian (RS)</a> |
        <a href="tag-format-i18ndemo.jsp?theLocale=el_GR">Greek (GR)</a>
        <hr>
        
        <h3><fmt:message key="label.greeting" /></h3>
        
        <p><fmt:message key="label.firstName" />: Mihajlo</p>
        <p><fmt:message key="label.lastName" />: Nesic</p>
        
        <h4><fmt:message key="label.welcome" /></h4>
        
        <hr>
        <p>Selected locale: ${theLocale}</p>
        
    </body>
</html>