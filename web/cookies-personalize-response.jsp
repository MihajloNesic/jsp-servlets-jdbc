<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirmation</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            // read form data
            String favLang = request.getParameter("favLang");
            
            // create the cookie
            Cookie cookie = new Cookie("jsptest.favLang", favLang);
            
            // set cookie lifespan (in seconds)
            cookie.setMaxAge(60*5); // 5 min
            
            // send cookie to browser
            response.addCookie(cookie);

        %>
        
        <h3>You have set your favorite language to: <%= favLang %></h3>
        <br>
        <a href="cookies.jsp">Return to homepage</a>
    </body>
</html>
