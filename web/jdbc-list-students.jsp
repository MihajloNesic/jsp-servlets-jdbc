<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JDBC CRUD app - Students list</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/jdbc-crud-app.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <h2>Test University</h2>
            </div>
        </div>
        <div id="container">
            <div id="content">
                <table border="1">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    <c:forEach var="student" items="${student_list}">
                        
                        <!-- Set up an update link for each student -->
                        <c:url var="updateLink" value="StudentControllerServlet">
                            <c:param name="command" value="LOAD"/>
                            <c:param name="studentID" value="${student.id}"/>
                        </c:url>
                        
                        <!-- Set up a delete link for each student -->
                        <c:url var="deleteLink" value="StudentControllerServlet">
                            <c:param name="command" value="DELETE"/>
                            <c:param name="studentID" value="${student.id}"/>
                        </c:url>
                        
                        <tr>
                            <td>${student.firstName}</td>
                            <td>${student.lastName}</td>
                            <td>${student.email}</td>
                            <td>
                                <a href="${updateLink}">Update</a>
                                |
                                <a href="${deleteLink}" onclick="if (!(confirm('Are you sure you want to delete this student?'))) return false">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                
                <input type="button" value="Add Student" onclick="window.location.href='jdbc-add-student.jsp'; return false;" class="add-student-button" />
            </div>
        </div>
        
        <p><a href="index.jsp">Back</a></p>
    </body>
</html>