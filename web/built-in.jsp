<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Built-In Objects</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h3>Request user agent: <%= request.getHeader("User-Agent") %></h3>
        <h3>Request language: <%= request.getLocale() %></h3>
    </body>
</html>
