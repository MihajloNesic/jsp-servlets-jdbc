<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Udemy Course</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <jsp:include page="header.html" />
        
        <p>Basics</p>
        <ul>
            <li><a href="expression.jsp">Expression</a></li>
            <li><a href="scriptlet.jsp">Scriptlet</a></li>
            <li><a href="declarations.jsp">Declaration</a></li>
            <li><a href="built-in.jsp">Built-In Objects</a></li>
        </ul>
        
        <p>Examples</p>
        <ul>
            <li><a href="homepage.jsp">Page with header and footer included</a></li>
            <li><a href="random-string.jsp">Random String</a></li>
            <li><a href="student-form.html">Student Form</a></li>
        </ul>
        
        <p>State Management</p>
        <ul>
            <li><a href="todolist.jsp">Sessions - To Do list app</a></li>
            <li><a href="cookies.jsp">Cookies - Favorite language app</a></li>
        </ul>
        
        <p>JSTL - JSP Standard Tag Library</p>
        <ul>
            <li><a href="tag-core-demo.jsp">Core tag - Demo (set, forEach)</a></li>
            <li><a href="tag-core-city.jsp">Core tag - City (forEach, if, choose)</a></li>
            <li><a href="tag-functions-demo.jsp">Functions tag - Demo (length, toUpperCase, startsWith)</a></li>
            <li><a href="tag-functions-demo2.jsp">Functions tag - Demo 2 (split, join)</a></li>
            <li><a href="tag-format-i18ndemo.jsp">Format tag - Internationalization (I18N) Demo (Multi-Lingual App)</a></li>
        </ul>
        
        <p>Servlets</p>
        <ul>
            <li><a href="TimeServlet">Server Time Demo</a></li>
            <li><a href="servlet-student.html">Student Form (GET and POST)</a></li>
            <li><a href="ParamServlet">Servlet Parameters (read configuration parameters from web.xml)</a></li>
        </ul>
        
        <p>MVC</p>
        <ul>
            <li><a href="MVCDemoServlet">MVC Demo - Student List</a></li>
            <li><a href="MVCDemoServlet2">MVC Demo 2 - Student List (better 'model')</a></li>
        </ul>
        
        <p>JDBC</p>
        <ul>
            <li><a href="ConnectionTestServlet">Connection Test</a></li>
            <li><a href="StudentControllerServlet">CRUD app - Student List</a></li>
        </ul>
        
        <jsp:include page="footer.jsp" />
    </body>
</html>
