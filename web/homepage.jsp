<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Homepage</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <jsp:include page="header.html" />
            
        <br><br><br>
        <p align="center">Homepage with header and footer</p>
        <br><br><br>
        
        <jsp:include page="footer.jsp" />
    </body>
</html>
