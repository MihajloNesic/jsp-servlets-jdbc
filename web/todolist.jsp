<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Sessions - To Do list</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <!-- Step 1: Create HTML form -->
        <form action="todolist.jsp">
            Add new item: <input type="text" name="theItem">
            <input type="submit" value="Add">
        </form>
        
        <!-- Step 2: Add new item to "To Do" list -->
        <%
            // get To Do items from the session
            List<String> items = (List<String>) session.getAttribute("todoList");
            
            //if the To Do items doesn't exist, then create a new one
            if(items == null) {
                items = new ArrayList<>();
                session.setAttribute("todoList", items);
            }
            // see if there is form data to add
            String theItem = request.getParameter("theItem");
            
            if(theItem != null) {
                items.add(theItem);
            }
        %>
        
        <!-- Step 3: Display all "To Do" items from session -->
        <hr>
        <b>To Do list items:</b><br>
        
        <ol>
            <%
                for(String item: items)
                    out.println("<li>"+item+"</li>");
            %>
        </ol>
        
    </body>
</html>
