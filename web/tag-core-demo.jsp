<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSTL Core Tag - Demo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <c:set var="time" value="<%= new java.util.Date() %>" />
        <h3>Time on the server is ${time}</h3>
        
        <%
            String[] cities = {"Prague", "Madrid", "Amsterdam"};
            pageContext.setAttribute("myCities", cities);
        %>
        <c:forEach var="city" items="${myCities}">
            ${city} <br>
        </c:forEach>
    </body>
</html>