CREATE DATABASE IF NOT EXISTS `udemy_jsp`;
USE `udemy_jsp`;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
INSERT INTO `student` VALUES 
(1,'Mary','Public','mary@test.com'),
(2,'John','Doe','john@test.co'),
(3,'Bill','Gates','bill@test.com'),
(4,'Maxwell','Dixon','max@test.com');

UNLOCK TABLES;